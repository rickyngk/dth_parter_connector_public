import requests
import json
import traceback
from akamai.edgegrid import EdgeGridAuth
from .dthalert import alert
import pytz, datetime

CONSOLE_TAG = "AKAMAI_API"

def urlencode(url):
    arr = url.split('?')
    q = ""
    if len(arr) > 0:
        q = ";".join(arr[1].split("&"))
    return f"<code>{arr[0]} query: {q}</code>"

class AkamaiAccess:
    host = None
    accessToken = None
    clientSecret = None
    clientToken = None
    def __init__(self, host, accessToken, clientSecret, clientToken):
        self.host = host
        self.accessToken = accessToken
        self.clientSecret = clientSecret
        self.clientToken = clientToken
        pass

class AkamaiInstance:
    apiCfg = None
    apiCcuCfg = None
    dthAlertChannel = None

    def __init__(self, apiCfg: AkamaiAccess, apiCcuCfg: AkamaiAccess = None, dthAlertChannel = None):
        self.apiCfg = apiCfg
        self.apiCcuCfg = apiCcuCfg
        self.dthAlertChannel = dthAlertChannel
        pass

    def telegramAlert(self, msg):
        if self.dthAlertChannel: alert(channel=self.dthAlertChannel, msg=msg)
    
    def __get(self, session, url, action = ""):
        try:
            r = session.get(url)
            print(CONSOLE_TAG, "[%s]"%(action), url, r.status_code)
            if r.status_code >= 200 and r.status_code < 400:
                data = json.loads(r.content.decode())
                return data
            else:
                print(urlencode(url))
                self.telegramAlert(f"<b>AKAMAI API FAIL {r.status_code}</b>\nGET {urlencode(url)}\nResp: <code>{r.content.decode()}</code>")
                print(f"{CONSOLE_TAG} [HTTP_ERROR]", r.status_code, r.content)
            return None
        except Exception as e:
            traceback.print_exc()
            self.telegramAlert(f"<b>AKAMAI API EXCEPTION</b>\nGET {urlencode(url)}\nException:<pre>{e.__class__.__name__}</pre>")
            raise(e)
        
    def __post(self, session, url, data, action = ""):
        try:
            r = session.post(url, json=data)
            print(CONSOLE_TAG, "[%s]"%(action), url, r.status_code)
            if r.status_code >= 200 and r.status_code < 400:
                data = json.loads(r.content.decode())
                return data
            else:
                self.telegramAlert(f"<b>AKAMAI API FAIL {r.status_code}</b>\nPOST {urlencode(url)}\nData: <pre>{data}</pre>\nResp: <code>{r.content.decode()}</code>")
                print(f"{CONSOLE_TAG} [HTTP_ERROR]", r.status_code, r.content)
            return None
        except Exception as e:
            traceback.print_exc()
            self.telegramAlert(f"<b>AKAMAI API EXCEPTION</b>\nPOST {urlencode(url)}\nData: <pre>{data}</pre>\nException:<pre>{e.__class__.__name__}</pre>")
            raise(e)

    def get(self, url, cnf:AkamaiAccess, action = ""):
        s = requests.Session()
        s.auth = EdgeGridAuth(
            client_token = cnf.clientToken,
            client_secret = cnf.clientSecret,
            access_token = cnf.accessToken
        )
        url = f"https://{cnf.host}{url}"

        hasMore = True
        offset = 0
        limit = 1000
        rows = []
        while hasMore:
            hasMore = False
            re = self.__get(s, f"{url}&limit={limit}&offset={offset}", action)
            if re:
                retRows = re.get("rows", [])
                meta = re.get("metaData", {})
                rows.extend(retRows)
                hasMore = meta.get("hasMoreData", False)
                offset += limit
        return rows

    def post(self, url, cnf:AkamaiAccess, data, action = ""):
        s = requests.Session()
        s.auth = EdgeGridAuth(
            client_token = cnf.clientToken,
            client_secret = cnf.clientSecret,
            access_token = cnf.accessToken
        )
        url = f"https://{cnf.host}{url}"

        re = self.__post(s, f"{url}", data, action)
        return re

    def realtimeMediaDeliveryReport(self, dimensions, metrics, startDate, endDate, reduce = False, aggregation = 300, action = "Media Delivery Report"):
        q = f"dimensions={dimensions}&metrics={metrics}&startDate={startDate}&endDate={endDate}&reduce={reduce and 'true' or 'false'}&aggregation={aggregation}"
        url = f"/media-delivery-reports/v1/adaptive-media-delivery/realtime-data?{q}"
        return self.get(url, self.apiCfg, action)
        
    def mediaDeliveryReport(self, dimensions, metrics, startDate, endDate, reduce = False, aggregation = 300, action = "Media Delivery Report"):
        q = f"dimensions={dimensions}&metrics={metrics}&startDate={startDate}&endDate={endDate}&reduce={reduce and 'true' or 'false'}&aggregation={aggregation}"
        url = f"/media-delivery-reports/v1/adaptive-media-delivery/data?{q}"
        return self.get(url, self.apiCfg, action)

    def downloadDeliveryReport(self, dimensions, metrics, startDate, endDate, reduce = False, aggregation = 300, action = "Download Delivery Report"):
        q = f"dimensions={dimensions}&metrics={metrics}&startDate={startDate}&endDate={endDate}&reduce={reduce and 'true' or 'false'}&aggregation={aggregation}"
        url = f"/media-delivery-reports/v1/download-delivery/data?{q}"
        return self.get(url,self.apiCfg, action)

    
    def __getAmdAvailableReports(self, id = None):
        url = "/media-delivery-reports/v1/adaptive-media-delivery/data-stores"
        if id != None:
            url += f"/{id}"
        return self.get(url, self.apiCfg, "AMD Available Reports")


    def getMediaDeliveryTraffic(self, year, month, day):
        dateObj = datetime.datetime(year, month, day, 0, 0, 0, tzinfo = pytz.utc)
        fromDate = dateObj - datetime.timedelta(hours=7)
        toDate = fromDate + datetime.timedelta(hours=24)

        originHitId = 167
        edgeHitId = 4
        midHitId = 9
        edgeVolumeId = 107
        midVolumeId = 116

        resp = self.mediaDeliveryReport('1,2', f"{originHitId},{edgeHitId},{midHitId},{edgeVolumeId},{midVolumeId},21,24,28,29,30", fromDate.strftime('%Y-%m-%dT%H:%M:%SZ'), toDate.strftime('%Y-%m-%dT%H:%M:%SZ'))

        # for ele in resp: print(ele)

        cpData = {}

        for v in resp:
            cp = v[1]
            if not (cp in cpData):
                cpData[cp] = []

            ts = int(v[0])
            tsDate = datetime.datetime.fromtimestamp(ts, tz=pytz.utc) + datetime.timedelta(hours=7)
            blockIndex = int((tsDate.hour * 3600 + tsDate.minute * 60 + tsDate.second)/300)
            
            originHit = int(v[2])
            edgeHit = int(v[3])
            midHit = int(v[4])
            edgeVolume = float(v[5])
            midVolume = float(v[6])
            totalReq = originHit + edgeHit + midHit
            totalHit = edgeHit + midHit

            cpData[cp].append({
                "ts": ts,
                "block": blockIndex,
                "requests": totalReq,
                "consumed": (edgeVolume + midVolume) * 1024,
                "miss": originHit,
                "hit": totalHit,
                "http_2xx": int(v[7]),
                "http_3xx": int(v[8]),
                "http_4xx": int(v[9]),
                "http_5xx": int(v[10]),
                "http_other": int(v[11])
            })

        return cpData

    def getDownloadDeliveryTraffic(self, year, month, day):
        dateObj = datetime.datetime(year, month, day, 0, 0, 0, tzinfo = pytz.utc)
        fromDate = dateObj - datetime.timedelta(hours=7)
        toDate = fromDate + datetime.timedelta(hours=24)

        originHitId = 167
        edgeHitId = 4
        midHitId = 9
        edgeVolumeId = 107
        midVolumeId = 116

        resp = self.downloadDeliveryReport('1,2', f"{originHitId},{edgeHitId},{midHitId},{edgeVolumeId},{midVolumeId},21,24,28,29,30", fromDate.strftime('%Y-%m-%dT%H:%M:%SZ'), toDate.strftime('%Y-%m-%dT%H:%M:%SZ'))

        # for ele in resp: print(ele)

        cpData = {}

        if not resp: return cpData

        for v in resp:
            cp = v[1]
            if not (cp in cpData):
                cpData[cp] = []

            ts = int(v[0])
            tsDate = datetime.datetime.fromtimestamp(ts, tz=pytz.utc) + datetime.timedelta(hours=7)
            blockIndex = int((tsDate.hour * 3600 + tsDate.minute * 60 + tsDate.second)/300)
            
            originHit = int(v[2])
            edgeHit = int(v[3])
            midHit = int(v[4])
            edgeVolume = float(v[5])
            midVolume = float(v[6])
            totalReq = originHit + edgeHit + midHit
            totalHit = edgeHit + midHit

            cpData[cp].append({
                "ts": ts,
                "block": blockIndex,
                "requests": totalReq,
                "consumed": (edgeVolume + midVolume) * 1024,
                "miss": originHit,
                "hit": totalHit,
                "http_2xx": int(v[7]),
                "http_3xx": int(v[8]),
                "http_4xx": int(v[9]),
                "http_5xx": int(v[10]),
                "http_other": int(v[11])
            })

        return cpData
        
    def getRealtimeMediaDeliveryTraffic(self, year, month, day):
        dateObj = datetime.datetime(year, month, day, 0, 0, 0, tzinfo = pytz.utc)
        fromDate = dateObj - datetime.timedelta(hours=7)
        toDate = fromDate + datetime.timedelta(hours=24)
        now = datetime.datetime.now(pytz.utc) - datetime.timedelta(minutes=15)
        if toDate > now:
            toDate = now

        originHitId = 612
        edgeHitId = 604
        midHitId = 608
        edgeVolumeId = 607
        midVolumeId = 611

        resp = self.realtimeMediaDeliveryReport('600,601', f"{originHitId},{edgeHitId},{midHitId},{edgeVolumeId},{midVolumeId}", fromDate.strftime('%Y-%m-%dT%H:%M:%SZ'), toDate.strftime('%Y-%m-%dT%H:%M:%SZ'))

        cpData = {}
        cpTsMapping = {}

        if not resp: return cpData

        for v in resp:
            # print(">>", v)
            cp = v[1]
            if not (cp in cpData):
                cpData[cp] = []
                cpTsMapping[cp] = {}

            ts = int(v[0])
            tsDate = datetime.datetime.fromtimestamp(ts, tz=pytz.utc) + datetime.timedelta(hours=7)
            blockIndex = int((tsDate.hour * 3600 + tsDate.minute * 60 + tsDate.second)/300)

            originHit = int(v[2])
            edgeHit = int(v[3])
            midHit = int(v[4])
            edgeVolume = float(v[5])
            midVolume = float(v[6])
            totalReq = originHit + edgeHit + midHit
            totalHit = edgeHit + midHit

            ele = {
                "ts": ts,
                "block": blockIndex,
                "requests": totalReq,
                "consumed": (edgeVolume + midVolume) * 1024,
                "miss": originHit,
                "hit": totalHit,
                "http_2xx": 0,
                "http_3xx": 0,
                "http_4xx": 0,
                "http_5xx": 0,
                "http_other": 0
            }
            cpData[cp].append(ele)
            cpTsMapping[cp][ts] = ele

        ## http status
        resp = self.realtimeMediaDeliveryReport('600,601', '617,618,619,620,616,621,622', fromDate.strftime('%Y-%m-%dT%H:%M:%SZ'), toDate.strftime('%Y-%m-%dT%H:%M:%SZ'))
        # print(__jsonDumps(resp))
        for v in resp:
            # print(">>", v)
            cp = v[1]
            if cp in cpData:
                ts = int(v[0])
                if ts in cpTsMapping[cp]:
                    ele = cpTsMapping[cp][ts]
                    ele["http_2xx"] = int(float(v[2]))
                    ele["http_3xx"] = int(float(v[3]))
                    ele["http_4xx"] = int(float(v[4]))
                    ele["http_5xx"] = int(float(v[5]))
                    ele["http_other"] = int(float(v[6]) + int(v[7]) + int(v[8]))


        return cpData

    def purgeCacheUriByDomain(self, domain, paths, protocol = "https"):
        if self.apiCcuCfg:
            if not isinstance(paths, list): paths = [str(paths)]
            for i in range(len(paths)): paths[i] = f"{protocol}://{domain}{paths[i]}"
            print(paths)
            return self.post("/ccu/v3/invalidate/url/production", self.apiCcuCfg, {
                "objects": paths
            }, "PURGE")
        else:
            raise Exception("MISSING_CCU_CONFIG")