import requests
import json
import traceback
from .dthalert import alert

CF_API_HOST="https://api.cloudflare.com/client/v4/"
CONSOLE_TAG = "CF_API"

class CloudflareInstance:
    apiKey = None
    dthAlertChannel = None
    host = None
    zoneId = None
    accEmail = None

    def __init__(self, apiKey, zoneId, accEmail, host = CF_API_HOST, dthAlertChannel = None):
        self.apiKey = apiKey
        self.host = host
        self.dthAlertChannel = dthAlertChannel
        self.zoneId = zoneId
        self.accEmail = accEmail

    def telegramAlert(self, msg):
        if self.dthAlertChannel: alert(channel=self.dthAlertChannel, msg=msg)


    def _req(self, method, action, data = None):
        headers = {
            "X-Auth-Email": self.accEmail,
            "X-Auth-Key": self.apiKey,
            "Content-Type": "application/json;charset=UTF-8"
        }
        url = "%s%s"%(self.host, action)
        try:
            if method == "get":
                r = requests.get(url, headers=headers)
            elif method == "post":
                r = requests.post(url, headers=headers, json=data)
            elif method == "put":
                r = requests.put(url, headers=headers, json=data)
            elif method == "delete":
                r = requests.delete(url, headers=headers, json=data)
            else:
                raise Exception("Invalid method")

            print(CONSOLE_TAG, "[%s]"%(action), url, headers, data, r.status_code, r.content)

            if r.status_code >= 200 and r.status_code < 400 and r.content:
                content = json.loads(r.content)
                return content
            else:
                self.telegramAlert(f"<b>CF API FAIL {r.status_code}</b>\n{method} {url}\nHeader: <pre>{json.dumps(headers)}</pre>\nData: <pre>{data}</pre>\nResp: <pre>{r.content}</pre>")
                print("[CF] [HTTP_ERROR]", r.status_code, r.content)
                raise Exception("CF__HTTP_ERROR")
        except Exception as e:
            if e and str(e) != "CF__HTTP_ERROR":
                traceback.print_exc()
                self.telegramAlert(f"<b>CF API EXCEPTION</b>\{method} {url}\nHeader: <pre>{json.dumps(headers)}</pre>\nData: <pre>{json.dumps(data)}</pre>\nException:<pre>{e.__class__.__name__}</pre>")
            raise(e)


    def _get(self, action):
        return self._req("get", action)

    def _post(self, action, data):
        return self._req("post", action, data)

    def getZoneId(self):
        return self.zoneId

    def listDNS(self):
        zoneId = self.getZoneId()
        if zoneId != None:
            tmp = self._get("zones/%s/dns_records"%(zoneId))
            if "result" in tmp:
                return tmp["result"]
        return []

    def createDNS(self, name, content, dnsType = "CNAME", ttl = 1, priority = 10, proxied = False):
        zoneId = self.getZoneId()
        if zoneId != None:
            tmp = self._post("zones/%s/dns_records"%(zoneId), {
                "type": dnsType,
                "name": name,
                "content": content,
                "ttl": ttl,
                "name": name,
                "priority": priority,
                "proxied": proxied
            })
            if "result" in tmp:
                return tmp["result"]
        return None

    def createCNAME(self, name, content, ttl = 1, priority = 10, proxied = False):
        return self.createDNS(name, content, "CNAME", ttl, priority, proxied)
    
    def purgeCacheUriByDomain(self, domain, paths, protocol = "https"):
        if not isinstance(paths, list): paths = [str(paths)]
        for i in range(len(paths)): paths[i] = f"{protocol}://{domain}{paths[i]}"
        zoneId = self.getZoneId()
        if zoneId != None:
            tmp = self._post("zones/%s/purge_cache"%(zoneId), {
                "files": paths
            })
            if "result" in tmp: return tmp["result"]
        return None