import requests
import json
from .dthalert import alert
import traceback
from .define import CDN_TYPE__LIVE, CDN_TYPE__WEB_ACC, CDN_TYPE__OBJ_DOWNLOAD, CDN_TYPE__VOD
from time import sleep
import pytz
from datetime import datetime, timedelta
import re

VNGCLOUD_HOST = "https://vcdn-api.vngcloud.vn/vcdn-api"
CONSOLE_TAG = "VNG_CLOUD_API"

def parseDate(_input):
    p = re.split(r'[\/\-\.]+', _input)
    d, m, y = int(p[0]), int(p[1]), int(p[2])
    return datetime(y, m, d, 0, 0, 0, 0)
class VngCloudInstance:
    apiToken = None
    dthAlertChannel = None
    host = None

    def __init__(self, apiToken, host = VNGCLOUD_HOST, dthAlertChannel = None):
        self.apiToken = apiToken
        self.host = host
        self.dthAlertChannel = dthAlertChannel

    def telegramAlert(self, msg):
        if self.dthAlertChannel: alert(channel=self.dthAlertChannel, msg=msg)

    def _header(self):
        return {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": f"Bearer {self.apiToken}"
        }

    def _post(self, api, data):
        url = f"{self.host}{api}"
        headers = self._header()
        r = None
        r = requests.post(url, headers=headers, json=data)
        print(f"{CONSOLE_TAG}", "POST", url, headers, data, r.status_code)

        if r.status_code >= 200 and r.status_code < 400:
            try:
                content = json.loads(r.content)
                # print(content)
                resp = content.get("data")
                success = content.get("success")
                if not success:
                    print(f"{CONSOLE_TAG} [FAIL]", content)
                    self.telegramAlert(f"<b>VNGCloud 2020 API FAIL</b>\nPOST {url}\nHeader: <pre>{json.dumps(headers)}</pre>\nData: <pre>{json.dumps(content)}</pre>")
                    raise Exception("VNG_CLOUD_API__FAIL")
                else:
                    return resp
            except Exception as e:
                if e and str(e) != "VNG_CLOUD_API__FAIL":
                    traceback.print_exc()
                    self.telegramAlert(f"<b>VNGCloud 2020 API EXCEPTION</b>\nPOST {url}\nHeader: <pre>{json.dumps(headers)}</pre>\nData: <pre>{json.dumps(data)}</pre>\nException:<pre>{e.__class__.__name__}</pre>")
                raise(e)
        else:
            self.telegramAlert(f"<b>VNGCloud 2020 API HTTP ERROR</b>\nPOST {url}\nHeader: <pre>{json.dumps(headers)}</pre>\nData: <pre>{json.dumps(data)}</pre>\nResponse status:{r.status_code}\nResponse content:<pre>{r.content}</pre>")
            print(f"{CONSOLE_TAG} [HTTP_ERROR]", r.status_code, r.content)
            raise Exception("VNG_CLOUD_API__ERROR")

    def _put(self, api, data):
        url = f"{self.host}{api}"
        headers = self._header()
        r = None
        r = requests.put(url, headers=headers, json=data)
        print(f"{CONSOLE_TAG}", "PUT", url, headers, data, r.status_code)

        if r.status_code >= 200 and r.status_code < 400:
            try:
                content = json.loads(r.content)
                resp = content.get("data")
                success = content.get("success")
                if not success:
                    print(f"{CONSOLE_TAG} [FAIL]", resp)
                    self.telegramAlert(f"<b>VNGCloud 2020 API FAIL</b>\PUT {url}\nHeader: <pre>{json.dumps(headers)}</pre>\nData: <pre>{json.dumps(content)}</pre>")
                    raise Exception("VNG_CLOUD_API__FAIL")
                else:
                    return resp
                
            except Exception as e:
                if e and str(e) != "VNG_CLOUD_API__FAIL":
                    traceback.print_exc()
                    self.telegramAlert(f"<b>VNGCloud 2020 API EXCEPTION</b>\PUT {url}\nHeader: <pre>{json.dumps(headers)}</pre>\nData: <pre>{json.dumps(data)}</pre>\nException:<pre>{e.__class__.__name__}</pre>")
                raise(e)
        else:
            self.telegramAlert(f"<b>VNGCloud 2020 API HTTP ERROR</b>\nPUT {url}\nHeader: <pre>{json.dumps(headers)}</pre>\nData: <pre>{json.dumps(data)}</pre>\nResponse status:{r.status_code}\nResponse content:<pre>{r.content}</pre>")
            print(f"{CONSOLE_TAG} [HTTP_ERROR]", r.status_code, r.content)
            raise Exception("VNG_CLOUD_API__ERROR")

    def _get(self, api):
        url = f"{self.host}{api}"
        headers = self._header()
        r = None
        r = requests.get(url, headers=headers)
        print(f"{CONSOLE_TAG}", "GET", url, headers, r.status_code)

        if r.status_code >= 200 and r.status_code < 400:
            try:
                content = json.loads(r.content)
                resp = content.get("data")
                success = content.get("success")
                if not success:
                    print(f"{CONSOLE_TAG} [FAIL]", resp)
                    self.telegramAlert(f"<b>VNGCloud 2020 API FAIL</b>\GET {url}\nHeader: <pre>{json.dumps(headers)}</pre>\nData: <pre>{json.dumps(content)}</pre>")
                    raise Exception("VNG_CLOUD_API__FAIL")
                else:
                    return resp
            except Exception as e:
                if e and str(e) != "VNG_CLOUD_API__FAIL":
                    traceback.print_exc()
                    self.telegramAlert(f"<b>VNGCloud 2020 API EXCEPTION</b>\GET {url}\nHeader: <pre>{json.dumps(headers)}</pre>\nException:<pre>{str(e)}</pre>")
                raise(e)
        else:
            self.telegramAlert(f"<b>VNGCloud 2020 API HTTP ERROR</b>\nPOST {url}\nHeader: <pre>{json.dumps(headers)}</pre>\nResponse status:{r.status_code}\nResponse content:<pre>{r.content}</pre>")
            print(f"{CONSOLE_TAG} [HTTP_ERROR]", r.status_code, r.content)
            raise Exception("VNG_CLOUD_API__ERROR") 

    def listCdns(self, cdnType = None):
        filterdCdnType = []
        ret = []
        if cdnType == None:
            filterdCdnType = [CDN_TYPE__LIVE, CDN_TYPE__WEB_ACC, CDN_TYPE__OBJ_DOWNLOAD, CDN_TYPE__VOD]
        elif not isinstance(cdnType, list):
            filterdCdnType = [cdnType]
        else:
            filterdCdnType = cdnType

        def addCndTypeLamda(ele, type):
            ele["type"] = type
            return ele
        for ele in filterdCdnType:
            re = None
            if ele == CDN_TYPE__LIVE:
                re = self._get("/v1/liv/cdn/list")
            elif ele == CDN_TYPE__WEB_ACC:
                re = self._get("/v1/webacc/list")
            elif ele == CDN_TYPE__OBJ_DOWNLOAD:
                re = self._get("/v1/obj-download/list")
            elif ele == CDN_TYPE__VOD:
                re = self._get("/v1/vod/list")
            if re:
                for item in re: item['objType'] = ele
                ret = ret + re
        return ret

    def getLiveCdns(self):
        return self.listCdns(cdnType=[CDN_TYPE__LIVE])

    def listLiveCdns(self):
        return self.listCdns(cdnType=[CDN_TYPE__LIVE])

    def findCdnByDomain(self, domain, cdnType = None):
        data = self.listCdns(cdnType)
        if data != None and len(data) > 0:
            for ele in data:
                if ele.get("cdnDomain") == domain:
                    return ele
        return None

    def findLiveCdnByDomain(self, domain):
        return self.findCdnByDomain(domain=domain, cdnType=[CDN_TYPE__LIVE])


    def getCdnDetailById(self, id, cdnType):
        if cdnType == CDN_TYPE__LIVE:
            api = f"/v1/liv/cdn/detail/{id}"
            re = self._get(api)
            if re: re["objType"] = CDN_TYPE__LIVE
            return re
        elif cdnType == CDN_TYPE__OBJ_DOWNLOAD:
            api = f"/v1/obj-download/detail/{id}"
            re = self._get(api)
            if re: re["objType"] = CDN_TYPE__LIVE
            return re
        elif cdnType == CDN_TYPE__VOD:
            api = f"/v1/vod/detail/{id}"
            re = self._get(api)
            if re: re["objType"] = CDN_TYPE__LIVE
            return re
        elif cdnType == CDN_TYPE__WEB_ACC:
            api = f"/v1/webacc/detail/{id}"
            re = self._get(api)
            if re: re["objType"] = CDN_TYPE__LIVE
            return re
        else:
            raise Exception("NOT_SUPPORT")

    def getCdnDetailByDomainName(self, domain):
        obj = self.findCdnByDomain(domain=domain)
        return self.getCdnDetailById(id=obj.get("cdnId"), cdnType=obj.get("objType"))

    def _updateCDN(self, cdnObj, putData = {}):
        obj = cdnObj.copy()
        if obj.get("objType") == CDN_TYPE__LIVE:
            api = f"/v1/liv/cdn/update"
            for k in putData:
                obj[k] = putData[k]
            return self._put(api, obj)
        else:
            raise Exception("NOT_SUPPORT")

    def _updateLiveCdnCNameByDomain(self, domain, cnames):
        if not isinstance(cnames, list):
            cnames = [cnames]
        cdnObj = self.getCdnDetailByDomainName(domain)
        if cdnObj:
            return self._updateCDN(cdnObj, {"cName": cnames})
        return None

    # important
    def updateLiveCdnCNameByDomain(self, domain, cnames):
        try:
            ret = self._updateLiveCdnCNameByDomain(domain, cnames)
            if not ret:
                print(f"{CONSOLE_TAG} updateLiveCdnCNameByDomainWithRetry >>> ERROR >>> Retry")
                sleep(3)
                return self._updateLiveCdnCNameByDomain(domain, cnames)
            return ret
        except:
            print(f"{CONSOLE_TAG} updateLiveCdnCNameByDomainWithRetry >>> EXCEPTION >>> Retry")
            sleep(3)
            return self._updateLiveCdnCNameByDomain(domain, cnames)
    

    # important
    def purgeCacheUriByDomain(self, domain, paths):
        if not isinstance(paths, list): paths = [str(paths)]
        obj = self.findCdnByDomain(domain=domain)
        if obj:
            objType = obj.get("objType")
            if objType != CDN_TYPE__LIVE:
                api = "/v1/cdn/flush-cache"
                return self._post(api, {
                    "cdnDomain": domain,
                    "patterns": paths,
                    "type": "URI"
                })
        return None

    #===============================
        
    def checkIsLimit(self, url):
        try:
            r = requests.get(url)
            if r.status_code >= 200 and r.status_code < 400:
                isLimit = r.headers.get('X-Limit')
                print(isLimit)
                return {
                    "success": True,
                    "isLimit": isLimit == "1" or isLimit == 1 or isLimit == "true"
                }
            else:
                return {
                    "success": False,
                    "err": f"[HTTP] {r.status_code}"
                }
        except Exception as e:
            return {
                "success": False,
                "err": f"[Exception] {e}"
            }

    def getRecentTraffic(self, domain):
        api = "/v1/analytic/traffic-consuming"
        data = {
            "period": "30m",
            "domains": [domain]
        }
        stat = self._post(api, data)
        if len(stat) > 0:
            sortedStat = sorted(stat)
            iter = []
            for k in sortedStat:
                v = stat[k]
                iter.append(int((v.get("cached", 0) + v.get("uncached", 0))/60))
            
            a = 1
            vec = []
            for i in range(len(iter) - 1):
                if iter[i] != None and iter[i] != 0 and iter[i + 1] != None and iter[i + 1] != 0:
                    diff = iter[i + 1]/iter[i]
                    vec.append(diff)

            if len(vec) == 1:
                a = vec[0]
            else:
                prevVec = vec[:-1]
                prevVec = prevVec[:4]
                a = 0.7 * vec[len(vec) - 1] + 0.3 * (sum(prevVec)/len(prevVec))

            recentStat = sortedStat[len(sortedStat)-1]
            recentValue = iter[len(iter) - 1]
            predictedValue = recentValue
            if a > 0: predictedValue = int(recentValue * a)
            return int(recentStat), recentValue, predictedValue, a
        return None, None, None, None
    
    def getTrafficReportLastHour(self, domain):
        api = "/v1/analytic/traffic-consuming"
        data = {
            "period": "1h",
            "domains": [domain]
        }
        stat = self._post(api, data)
        re = {}
        for k in sorted(stat):
            ts = int(k)
            dateTimeObj = datetime.fromtimestamp(ts / 1000, pytz.utc) + timedelta(hours=7)
            dateKey = f"{dateTimeObj.year}:{str(dateTimeObj.month).zfill(2)}:{str(dateTimeObj.day).zfill(2)}"
            block = int((dateTimeObj.hour * 60 + dateTimeObj.minute) / 5)
            bw = (stat[k].get("cached", 0) or 0) + (stat[k].get("uncached", 0) or 0)
            key = f"vng2-domain-stat-traffic:{domain}:{dateKey}"
            if not key in re: re[key] = {}
            re[key][block] = bw
        return re


    def getTrafficReportAtHour(self, domain, day, month, year, hour):
        if hour >= 0 and hour <= 23:
            api = "/v1/analytic/traffic-consuming"
            data = {
                "fromTime": f"{day:02}/{month:02}/{year} {hour:02}:00:00",
                "toTime": f"{day:02}/{month:02}/{year} {(hour+1):02}:00:00",
                "domains": [domain]
            }
            print("[getTrafficReportAtHour]", data)
            stat = self._post(api, data)
            re = {}
            for k in sorted(stat):
                ts = int(k)
                dateTimeObj = datetime.fromtimestamp(ts / 1000, pytz.utc) + timedelta(hours=7)
                dateKey = f"{dateTimeObj.year}:{str(dateTimeObj.month).zfill(2)}:{str(dateTimeObj.day).zfill(2)}"
                block = int((dateTimeObj.hour * 60 + dateTimeObj.minute) / 5)
                bw = (stat[k].get("cached", 0) or 0) + (stat[k].get("uncached", 0) or 0)
                key = f"vng2-domain-stat-traffic:{domain}:{dateKey}"
                if not key in re: re[key] = {}
                re[key][block] = bw
            return re
        else:
            print("[getTrafficReportAtHour]", f"INVALID HOUR {hour}")
            return {}

    def getTrafficReportNHoursPrev(self, domain, nHour):
        print("[getTrafficReportNHoursPrev]", domain, nHour)
        timepoint = datetime.now(pytz.utc) + timedelta(hours=7) - timedelta(hours=nHour)
        return self.getTrafficReportAtHour(domain, timepoint.day, timepoint.month, timepoint.year, timepoint.hour)

    def getRequestReportLastHour(self, domain):
        api = "/v1/analytic/cdn-requestsps"
        data = {
            "period": "1h",
            "domains": [domain]
        }
        stat = self._post(api, data)
        re = {}
        for k in sorted(stat):
            ts = int(k)
            dateTimeObj = datetime.fromtimestamp(ts / 1000, pytz.utc) + timedelta(hours=7)
            dateKey = f"{dateTimeObj.year}:{str(dateTimeObj.month).zfill(2)}:{str(dateTimeObj.day).zfill(2)}"
            block = int((dateTimeObj.hour * 60 + dateTimeObj.minute) / 5)
            cachedData = stat[k].get("cached", 0)
            uncachedData = stat[k].get("uncached", 0)
            if cachedData != None and uncachedData != None:
                bw = cachedData + uncachedData
                key = f"vng2-domain-stat-requests:{domain}:{dateKey}"
                if not key in re: re[key] = {}
                re[key][block] = bw
        return re

    def getRequestReportAtHour(self, domain, day, month, year, hour):
        if hour >= 0 and hour <= 23:
            api = "/v1/analytic/cdn-requestsps"
            data = {
                "fromTime": f"{day:02}/{month:02}/{year} {hour:02}:00:00",
                "toTime": f"{day:02}/{month:02}/{year} {(hour+1):02}:00:00",
                "domains": [domain]
            }
            print("[getRequestReportAtHour]", data)
            stat = self._post(api, data)
            re = {}
            for k in sorted(stat):
                ts = int(k)
                dateTimeObj = datetime.fromtimestamp(ts / 1000, pytz.utc) + timedelta(hours=7)
                dateKey = f"{dateTimeObj.year}:{str(dateTimeObj.month).zfill(2)}:{str(dateTimeObj.day).zfill(2)}"
                block = int((dateTimeObj.hour * 60 + dateTimeObj.minute) / 5)
                cachedData = stat[k].get("cached", 0)
                uncachedData = stat[k].get("uncached", 0)
                if cachedData != None and uncachedData != None:
                    bw = cachedData + uncachedData
                    key = f"vng2-domain-stat-requests:{domain}:{dateKey}"
                    if not key in re: re[key] = {}
                    re[key][block] = bw
            return re
        else:
            print("[getRequestReportAtHour]", f"INVALID HOUR {hour}")
            return {}

    def getRequestReportNHoursPrev(self, domain, nHour):
        print("[getRequestReportNHoursPrev]", domain, nHour)
        timepoint = datetime.now(pytz.utc) + timedelta(hours=7) - timedelta(hours=nHour)
        return self.getRequestReportAtHour(domain, timepoint.day, timepoint.month, timepoint.year, timepoint.hour)

    def cacheStatus(self, domain, fromDate, toDate):
        api = "/v1/analytic/cache-status"
        fromDateObj = parseDate(fromDate) - timedelta(hours=7)
        toDateObject = parseDate(toDate) + timedelta(days=1) - timedelta(hours=7) - timedelta(seconds=1)
        now = datetime.today() - timedelta(seconds=1)
        if (toDateObject > now): toDateObject = now

        data = {
            "fromTime": f"{str(fromDateObj.day).zfill(2)}/{str(fromDateObj.month).zfill(2)}/{str(fromDateObj.year).zfill(2)} {str(fromDateObj.hour).zfill(2)}:00:00",
            "toTime": f"{str(toDateObject.day).zfill(2)}/{str(toDateObject.month).zfill(2)}/{str(toDateObject.year).zfill(2)} {str(toDateObject.hour).zfill(2)}:00:00",
            "domains": [domain]
        }
        stat = self._post(api, data)
        if stat != None:
            re = json.loads(stat)
            if re:
                return {
                    "miss": re.get("miss", 0) or 0,
                    "hit": re.get("hit", 0) or 0
                }
        else:
            return {
                "miss": 0,
                "hit": 0
            }

    def httpCodes(self, domain, fromDate, toDate):
        api = "/v1/analytic/cdn-http-codes"
        fromDateObj = parseDate(fromDate) - timedelta(hours=7)
        toDateObject = parseDate(toDate) + timedelta(days=1) - timedelta(hours=7) - timedelta(seconds=1)
        now = datetime.today() - timedelta(seconds=1)
        if (toDateObject > now): toDateObject = now

        data = {
            "fromTime": f"{str(fromDateObj.day).zfill(2)}/{str(fromDateObj.month).zfill(2)}/{str(fromDateObj.year).zfill(2)} {str(fromDateObj.hour).zfill(2)}:00:00",
            "toTime": f"{str(toDateObject.day).zfill(2)}/{str(toDateObject.month).zfill(2)}/{str(toDateObject.year).zfill(2)} {str(toDateObject.hour).zfill(2)}:00:00",
            "domains": [domain]
        }
        stat = self._post(api, data)
        if stat != None:
            re = json.loads(stat)
            if re:
                return re
        else:
            return {}
