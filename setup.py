from setuptools import setup

setup(
    name='dth_partner_connector',
    version='0.0.1',
    description='My private package from private github repo',
    url='https://rickyngk@bitbucket.org/rickyngk/parter_connector.git',
    author='DN',
    author_email='duy.nguyen@dthcdn.com',
    license='unlicense',
    packages=['dth_partner_connector'],
    zip_safe=False,
    install_requires=[
        'requests',
        'pytz',       
        'edgegrid-python'         
    ],
)